#ifndef __FREERTOSESP32_H__
#define __FREERTOSESP32_H__

#include <Arduino.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>

extern portMUX_TYPE mux;

class FreeRTOSESP32 {
public:
  FreeRTOSESP32() {};
  void start(TaskFunction_t fn, void * const arg=NULL, const uint32_t StackDepth=2048, uint8_t core_no=1);
  void stop();
  void resume();
  void delay(int delayms);
  void delayUntil(int freq);
  void destroy();
  void noInterrupt();
  void interrupt();
    
  FreeRTOSESP32* ptr = const_cast<FreeRTOSESP32*>(this);
private:
  TaskHandle_t  xHandle;
};

#endif //__FREERTOSESP32_H__