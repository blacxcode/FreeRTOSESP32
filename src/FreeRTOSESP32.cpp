#include "FreeRTOSESP32.h"

void FreeRTOSESP32::start( TaskFunction_t fn, void * const arg, const uint32_t StackDepth, uint8_t core_no) {
  String task_name = String("FreeRTOS_Task_")+ String(random(10000));

  if(core_no>1) core_no = 1;
  
  xTaskCreatePinnedToCore(fn, task_name.c_str(), StackDepth, arg, 5, &ptr->xHandle, core_no); 
  //xTaskCreate( fn, task_name.c_str(),StackDepth, arg, 5, &ptr->xHandle); 
}

void FreeRTOSESP32::stop()   { 
	if(xHandle != NULL) {
		vTaskSuspend(xHandle); 
	}
}

void FreeRTOSESP32::resume() { 
	if(xHandle != NULL) {
		vTaskResume(xHandle); 
	}
}

void FreeRTOSESP32::delay(int delayms) { 
	const TickType_t xDelay = delayms / portTICK_PERIOD_MS;
	
	vTaskDelay(xDelay);
}

void FreeRTOSESP32::delayUntil(int freq) { 
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = freq;
	xLastWakeTime = xTaskGetTickCount();
	
	vTaskDelayUntil(&xLastWakeTime, xFrequency);
}

void FreeRTOSESP32::destroy() { 
	if(xHandle != NULL) {
         vTaskDelete(xHandle);
	}
}

void FreeRTOSESP32::noInterrupt() {
	taskENTER_CRITICAL_ISR(&mux);
}

void FreeRTOSESP32::interrupt() {
	taskEXIT_CRITICAL_ISR(&mux);
}
  
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;
